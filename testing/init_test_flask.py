import os
import subprocess
import time

from space import *


VERSION = 'mysql:8.0.29'

#get list of all container ID's, stop all continers by ID
os.system("sudo docker ps -aq |sudo xargs docker stop")

#delete all images cached data
os.system("sudo docker system prune -a -f")

#alternative just delete flask_db container for the rebuild
#Fixes any update conflits for repeated testing
if "flask_db" in str(subprocess.check_output("sudo docker container ls -a", shell=True)):
    os.system("sudo docker container rm flask_db")

start_space = getAvailSpace()
#start logging data
log = open('/home/pi/project/testing/init_log.txt', 'w+')
log.write('%s gb of available space' % start_space)

outvmstat = open('/home/pi/project/testing/vmstat.txt', 'w+')
memstat = subprocess.Popen(["vmstat", "1"], stdout=outvmstat)

#start time measure for build and up
start_time = time.time()

try:
    #make build with older mqsql target
    replaceVersion(VERSION)    
    #set directory, build and compose
    init = subprocess.run("cd /home/pi/project/flask_docker && sudo docker compose up -d --build", shell=True)
except Exception as e:
    memstat.terminate()
    log.write("\n" + str(e))

outvmstat.close()
memstat.terminate()

#finilize loggin and output to file
end_time = time.time() - start_time
end_space = start_space - getAvailSpace()

log.write('\n%s seconds to complete build and docker compose up' % end_time)
log.write('\nTotal space used: %s'% end_space)
log.close()