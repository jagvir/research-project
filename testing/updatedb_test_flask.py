import os
import subprocess
import time

from space import *

VERSION = 'mysql:8.0.34'

start_space = getAvailSpace()
log = open('/home/pi/project/testing/update_log.txt', 'w+')
log.write('%s gb of available space' % start_space)

try:
    #check if database is operatinal
    out = subprocess.check_output("sudo docker exec flask_db mysqladmin -proot -h localhost status", shell=True)
    if "Uptime" in str(out):
        outvmstat = open('/home/pi/project/testing/update_vmstat.txt', 'w+')
        memstat = subprocess.Popen(["vmstat", "1"], stdout=outvmstat)
        # currentVersion = str(subprocess.check_output("sudo docker exec flask_db mysql --version", shell=True))[2:-3]
        # log.write("\nCurrent db version: " + currentVersion)
        start_time = time.time()
        #pull updated db image before running docker compose up, minimize downtime
        subprocess.run("sudo docker pull " + VERSION, shell=True)
        os.system("sudo docker stop flask_db")
        replaceVersion(VERSION)
        subprocess.run("cd /home/pi/project/flask_docker && sudo docker compose up -d", shell=True)
        end_time = time.time() - start_time
        end_space = start_space - getAvailSpace()
        # updatedVersion = str(subprocess.check_output("sudo docker exec flask_db mysql --version", shell=True))[2:-3]
        # log.write("\nUpdated db to: " + updatedVersion)
        log.write('\n%s seconds update db' % end_time)
        log.write('\nTotal space used for update: %s'% end_space)
        memstat.terminate()
        outvmstat.close()
        log.close()
except:
    print("db connection could not be establshed")