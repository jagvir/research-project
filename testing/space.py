import psutil
import yaml


def getAvailSpace():
    path = '/'
    bytes_avail = psutil.disk_usage(path).free
    gigabytes_avail = bytes_avail / 1024 / 1024 / 1024
    return float(gigabytes_avail)

#open file and replace db verion to new
def replaceVersion(ver):
    with open('/home/pi/project/flask_docker/docker-compose.yml', 'r') as dockercompose_stream:
        try:
            data = yaml.safe_load(dockercompose_stream)
        except yaml.YAMLError as e:
            print(e)
            
    data['services']['db']['image'] = ver
    
    with open('/home/pi/project/flask_docker/docker-compose.yml', 'w') as dockercompose_stream:
        try:
            yaml.safe_dump(data, dockercompose_stream, default_flow_style=False, sort_keys=False)
        except yaml.YAMLError as e:
            print(e)