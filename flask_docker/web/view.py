from flask import Flask, render_template
from typing import List, Dict
import os, json, mysql.connector

app = Flask(__name__)
app.secret_key = "unicorns"

def test_table() -> List[Dict]:
    config = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'devopsroles'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM test_table')
    results = [{name: color} for (name, color) in cursor]
    cursor.close()
    connection.close()
    return results

@app.route('/')
def index():
   return render_template("index.html")

@app.route('/t')
def db() -> str:
    return json.dumps({'test_table': test_table()})

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)